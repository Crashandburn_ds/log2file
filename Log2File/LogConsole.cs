﻿using System;
using System.IO;
using System.Text;

namespace Log2File
{
    public class LogConsole : TextWriter
    {
        public bool isLogging = false;

        public override Encoding Encoding { get { return Encoding.UTF8; } }

        public override void Write(string value)
        {
            Log.Debug(value);
            base.Write(value);
        }

        public override void WriteLine(string value)
        {

            Log.Debug(value);
            base.WriteLine(value);
        }
    }
}
