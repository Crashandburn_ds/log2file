﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Log2File
{
    public static class Log
    {
        public static int MAX_FILE_SIZE_MB = 50;
        public static int MAX_FILE_COUNT = 10;

        private static string _failSafePath = Path.Combine(Environment.CurrentDirectory, @"logging.log");

        private static string _logPath = Path.Combine(Environment.CurrentDirectory, @"logging.log");

        /// <summary>
        /// Gets or Sets the current path to the logfile
        /// </summary>
        public static string logPath
        {
            get
            {
                return _logPath;
            }
            set
            {
                _logPath = value;
            }
        }


        private static bool _isLoggingEnabled = false;
        /// <summary>
        /// Returns wether logging has been enabled or disabled
        /// </summary>
        public static bool isLoggingEnabled
        {
            private set
            {
                _isLoggingEnabled = value;
            }

            get
            {
                return _isLoggingEnabled;
            }
        }

        private static bool _isBlackBoxEnabled = false;
        /// <summary>
        /// Returns wether black box logging has been enabled or disabled
        /// </summary>
        public static bool isBlackBoxEnabled
        {
            private set
            {
                _isBlackBoxEnabled = value;
            }

            get
            {
                return _isBlackBoxEnabled;
            }
        }

        private static LogConsole logConsole = new LogConsole();

        #region Public Methods

        /// <summary>
        /// Enables Logging to file
        /// </summary>
        public static void EnableLogging()
        {
            isLoggingEnabled = true;
        }

        /// <summary>
        /// Disables logging to file
        /// </summary>
        public static void DisableLogging()
        {
            isLoggingEnabled = false;
        }

        /// <summary>
        /// Enables Blackbox Logging to file
        /// </summary>
        public static void EnableBlackBox()
        {
            isBlackBoxEnabled = true;
        }

        /// <summary>
        /// Disables logging to file
        /// </summary>
        public static void DisableBlackBox()
        {
            isBlackBoxEnabled = false;
        }

        /// <summary>
        /// Writes the given object / string to file with timestamp (Debug Classification)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static void Debug<T>(T obj)
        {
            if (obj is UnhandledExceptionEventArgs)
                Write(logLevels.Debug, obj as UnhandledExceptionEventArgs);
            else if (obj is Exception)
                Write(logLevels.Debug, obj as Exception);
            else
                Write(logLevels.Debug, obj.ToString());
        }

        /// <summary>
        /// Writes the given object / string to file with timestamp (Information Classification)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static void Information<T>(T obj)
        {
            if (obj is UnhandledExceptionEventArgs)
                Write(logLevels.Information, obj as UnhandledExceptionEventArgs);
            else if (obj is Exception)
                Write(logLevels.Information, obj as Exception);
            else
                Write(logLevels.Information, obj.ToString());
        }

        /// <summary>
        /// Writes the given object / string to file with timestamp (Error Classification)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static void Error<T>(T obj)
        {
            if (obj is UnhandledExceptionEventArgs)
                Write(logLevels.Error, obj as UnhandledExceptionEventArgs);
            else if (obj is Exception)
                Write(logLevels.Error, obj as Exception);
            else
                Write(logLevels.Error, obj.ToString());
        }

        /// <summary>
        /// Writes the given object / string to file with timestamp (Warning Classification)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static void Warning<T>(T obj)
        {
            if (obj is UnhandledExceptionEventArgs)
                Write(logLevels.Warning, obj as UnhandledExceptionEventArgs);
            else if (obj is Exception)
                Write(logLevels.Warning, obj as Exception);
            else
                Write(logLevels.Warning, obj.ToString());
        }

        /// <summary>
        /// Writes the given object / string to file with timestamp (Critical Classification)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static void Critical<T>(T obj)
        {
            if (obj is UnhandledExceptionEventArgs)
                Write(logLevels.Critical, obj as UnhandledExceptionEventArgs);
            else if (obj is Exception)
                Write(logLevels.Critical, obj as Exception);
            else
                Write(logLevels.Critical, obj.ToString());
        }

        /// <summary>
        /// Writes the given object / string to file with timestamp (Fatal Classification)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public static void Fatal<T>(T obj)
        {
            if (obj is UnhandledExceptionEventArgs)
                Write(logLevels.Fatal, obj as UnhandledExceptionEventArgs);
            else if (obj is Exception)
                Write(logLevels.Fatal, obj as Exception);
            else
                Write(logLevels.Fatal, obj.ToString());
        }

        /// <summary>
        /// Log console output to file
        /// </summary>
        /// <param name="enable">
        /// True, Enable Logging
        /// False, Disable Logging
        /// </param>
        public static void LogConsole(bool enable = true)
        {
            Console.SetOut(enable ? logConsole : Console.Out);
        }
        #endregion

        #region Private Methods
        private enum logLevels
        {
            Debug = 0,
            Information = 1,
            Error = 2,
            Warning = 3,
            Critical = 4,
            Fatal = 5
        }

        /// <summary>
        /// Writes string messages to logfile with timestamp and classification
        /// </summary>
        /// <param name="_level"></param>
        /// <param name="_message"></param>
        private static void Write(logLevels _level, string _message)
        {
            if (_isLoggingEnabled)
            {
                CheckFileLength(_logPath);
                try
                {
                    using (StreamWriter writer = new StreamWriter(_logPath, true))
                    {
                        writer.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        writer.WriteLine("[{0}] {1}", _level.ToString(), _message);
                    }
                }
                catch (Exception ex)
                {
                    DefaultToFailsafe(ex);
                }
            }
        }

        /// <summary>
        /// Writes exceptions to log file with timestamp and classification
        /// </summary>
        /// <param name="_level"></param>
        /// <param name="_e"></param>
        private static void Write(logLevels _level, Exception _e)
        {
            if (_isLoggingEnabled)
            {
                CheckFileLength(_logPath);
                try
                {
                    using (StreamWriter writer = new StreamWriter(_logPath, true))
                    {
                        writer.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        writer.WriteLine("[{0}]", _level.ToString());
                        if (_e != null)
                        {
                            writer.WriteLine("Message:{0} \n StackTrace:{1}", _e.Message, _e.StackTrace);
                        }
                    }
                }
                catch (Exception ex)
                {
                    DefaultToFailsafe(ex);
                }
            }
        }

        /// <summary>
        /// Writes Unhandled exceptions to log file with timestamp and classification
        /// </summary>
        /// <param name="_level"></param>
        /// <param name="_e"></param>
        private static void Write(logLevels _level, UnhandledExceptionEventArgs _e)
        {
            if (_isLoggingEnabled)
            {
                CheckFileLength(_logPath);
                try
                {
                    using (StreamWriter writer = new StreamWriter(_logPath, true))
                    {
                        writer.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        writer.WriteLine("[{0}]", _level.ToString());
                        if (_e != null)
                        {
                            writer.WriteLine("Message:{0} \n Terminating:{1}", _e.ExceptionObject, _e.IsTerminating);
                        }
                    }
                }
                catch (Exception ex)
                {
                    DefaultToFailsafe(ex);
                }
            }
        }

        /// <summary>
        /// Checks size of current logfile and archives old larger than file limit.
        /// </summary>
        /// <param name="logPath"></param>
        private static void CheckFileLength(string logPath)
        {
            try
            {
                long length = new System.IO.FileInfo(logPath).Length;

                if (length >= MAX_FILE_SIZE_MB * 1048576)
                {
                    string extension = Path.GetExtension(logPath);

                    string date = String.Format("{0}{1}{2}{3}{4}{5}", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"), DateTime.Now.Hour.ToString("00"), DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"));
                    string archivePath = string.Format("{0}{1}", StripExtensionAndNumbering(logPath), string.Format("-{0}{1}", date, extension));

                    Console.WriteLine("moving " + _logPath + " to " + archivePath);

                    File.Move(_logPath, archivePath);

                    CheckFileCount(_logPath);
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Deletes the oldest log file in the log directory 
        /// </summary>
        /// <param name="_logDirectory"></param>
        private static void CheckFileCount(string _logDirectory)
        {
            ///Gets directory only not file name
            _logDirectory = Path.GetDirectoryName(_logDirectory);

            string targetFile = StripExtensionAndNumbering(_logDirectory);

            List<string> filesInDirectory = new List<string>(Directory.GetFiles(_logDirectory));

            foreach (string file in filesInDirectory)
            {
                string filename = StripExtensionAndNumbering(file);
                if (!filename.Contains(targetFile))
                {
                    filesInDirectory.Remove(file);
                }
            }

            if(filesInDirectory.Count > MAX_FILE_COUNT)
            {
                DateTime lastModified = DateTime.MaxValue;
                string oldestFile = "";

                foreach (string file in filesInDirectory)
                {
                    FileInfo info = new FileInfo(file);

                    if(info.LastWriteTime < lastModified)
                    {
                        lastModified = info.LastWriteTime;
                        oldestFile = file;
                    }
                }

                if(oldestFile != "")
                {
                    try
                    {
                        File.Delete(oldestFile);
                    }catch(Exception ex)
                    {

                    }
                }
            }
        }

        /// <summary>
        /// Removes .log extention and numbering
        /// </summary>
        /// <param name="logPath"></param>
        /// <returns>Returns Filename without numbering or extension</returns>
        private static string StripExtensionAndNumbering(string logPath)
        {
            logPath = Path.ChangeExtension(logPath, null);

            if (Path.GetFileName(logPath).Contains("-"))
            {
                int stripIndex = logPath.LastIndexOf("-");
                if(stripIndex >= 0)
                {
                    logPath = logPath.Substring(0, stripIndex);
                }
            }

            return logPath;
        }

        /// <summary>
        /// Sets the logpath to a default path and records the new path in eventviewer.
        /// </summary>
        /// <param name="ex"></param>
        private static void DefaultToFailsafe(Exception ex)
        {
            using (EventLog eventLog = new EventLog("Log2File"))
            {
                eventLog.Source = "Log2File";
                eventLog.WriteEntry(string.Format("Cannot write to defined file! Writing to failsafe instead {0}\n{1}", _failSafePath, ex), EventLogEntryType.Error, 102, 1);
            }

            _logPath = _failSafePath;
        }
        #endregion

        #region EventHandlers

        /// <summary>
        /// Exception Handler for unhandledExceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            ///Set tempstate with current isLogging value
            bool _tempLoggingState = _isLoggingEnabled;

            ///If blackbox is enabled and logging is disabled then enable
            if (isBlackBoxEnabled && !_tempLoggingState)
                _isLoggingEnabled = true;

            ///Log Fatal Exception
            Fatal(e);

            ///If blackbox is enabled and logging was disabled then reset back to this state.
            if (isBlackBoxEnabled && !_tempLoggingState)
                _isLoggingEnabled = false;
        }

        /// <summary>
        /// Event Handler for thread exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void ThreadExceptionEventHandler(object sender, ThreadExceptionEventArgs e)
        {
            ///Set tempstate with current isLogging value
            bool _tempLoggingState = _isLoggingEnabled;

            ///If blackbox is enabled and logging is disabled then enable
            if(isBlackBoxEnabled && !_tempLoggingState)
                _isLoggingEnabled = true;

            ///Log Critical Exception
            Critical(e);

            ///If blackbox is enabled and logging was disabled then reset back to this state.
            if(isBlackBoxEnabled && !_tempLoggingState)
                _isLoggingEnabled = false;
        }

        #endregion
    }
}
