# Log2File

A simple implementation for logging error messages and exceptions to a file.


## Usage

Add Reference to library
```
using Log2File;
```

Attach to Catch any unhandled Exceptions
```
AppDomain.CurrentDomain.UnhandledException += Log.UnhandledExceptionHandler;
```

Enable logging (Enables all logging)
```
Log.EnableLogging();
```

Enable Blackbox logging (Enables unhandled Exception logging but nothing else)
```
Log.EnableBlackbox();
```

Change log location
```
Log.logPath = "{path to file}";
```

Change maximum log file size 
```
//Enter max file size in MegaBytes (Defaults to 50MB)
Log.MAX_FILE_SIZE_MB = 50;
```

Change maximum log count 
```
//Enter max amount of log files to be saved (Defaults to 10)
Log.MAX_File_COUNT = 10;
```

Log informational message
```
Log.Information("Application Started");
```

Log debug message
```
Log.Debug("Application Started");
```

Log error message
```
Log.Error("Null reference exception occured");
```

Log Warning message
```
Log.Warning("File size is nearing limit");
```

Log Critical message
```
Log.Critical("File could not be saved");
```

Log Fatal message
```
Log.Fatal("An Unhandled Exception occured");
```

Log all console messages to file
```
Log.LogConsole(true);
```

